 History of Commits
=========================
*A history of knowledge gained*  
*Add new knowledge to the end*

## Python Cmake Notes
 * src folder will be the module which all code is run from so make sure that this is named something other than `src`
 * module folder (the one not named `src`) is installed in usr/lib/python3/dist-packages.
    * Copy files individually because you don't want to copy over all the cache junk
 * Make application.py in module which launches application
 * make an extensionless file in the top directory that simply runs the application.py file from the module

## Commit Notes
 * Updated css so that it is more consistent on all themes
    >Turns out that elementary does some stuff with .list-row and .button classes that change how the sidebar looked. Without the elementary theme, the sidebar wasn't laid out correctly. I just added some css that made things more consistent across other themes so that it looks about the same regardless of the theme.
