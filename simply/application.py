#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  application.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#!/usr/bin/python3.4

from gi.repository import Gtk, Gio, GLib, Gdk
from simply.window.portraitWindow import PortraitWindow
import simply.config as config

#application class
class Application(Gtk.Application):
    #init function
    def __init__(self):
        Gtk.Application.__init__(self, application_id='org.gnome.Simply',
                                 flags=Gio.ApplicationFlags.FLAGS_NONE)
        GLib.set_application_name("Simply")
        GLib.set_prgname("simply")

        add_stylesheet("style/style.css")

        self._window = None

    def do_activate(self):
        if not self._window:
            self._window = PortraitWindow()

        self._window.present()
        Gtk.main()

#init style sheets
def add_stylesheet(path):
    provider = Gtk.CssProvider()
    css_file = config.PKG_DATADIR + "/" + path
    try:
        provider.load_from_path(css_file) #load css file
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
        print("Loaded %s" % css_file)
    except Exception as e:
        print("Error loading %s" % css_file)
