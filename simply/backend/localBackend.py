#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  core/localBackend.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import sqlite3
from os.path import expanduser, join, isdir
from os import makedirs
from simply.backend.backendBase import BackendBase
import simply.config as config

class LocalBackend(BackendBase):

    db_path = ""

    def __init__(self):
        super(BackendBase, self).__init__() #init class

        self.init_database()

    def init_database(self):
        #if (config.DEBUG): #if running from local directories
        if (False):
            db_path = config.DATA_BASE
        else: #not running from local directories
            #create path for user data
            user_data_dir = expanduser(join("~", ".local", "share", config.GETTEXT_PACKAGE, "data"))
            self.db_path = join(user_data_dir, "simply.db") #create path that points to actual database

            if (not isdir(user_data_dir)): #if data directory doesn't exist then create one
                makedirs(user_data_dir)
                print("Created User Directory")

        print(self.db_path)

        conn = sqlite3.connect(self.db_path)
        cursor = conn.cursor()

        cursor.execute(commands.NEW_DATABASE)
        conn.commit()

        cursor.close()
        conn.close()

""" Class of Sqlite3 commands """
class commands(object):
    NEW_DATABASE = """
        CREATE TABLE IF NOT EXISTS Notes (
            id       INTEGER PRIMARY KEY AUTOINCREMENT,
			time     TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
			title    TEXT                                NOT NULL,
            content  BLOB,
            tags     STRING,
            priority INTEGER
		);
    """
