#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  config.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from os.path import join

DATADIR = "@DATADIR@"
PKG_DATADIR = "@PKG_DATADIR@"
GETTEXT_PACKAGE = "@GETTEXT_PACKAGE@"
RELEASE_NAME = "@RELEASE_NAME@"
VERSION = "@VERSION@"
VERSION_INFO = "@VERSION_INFO@"
INSTALL_PREFIX = "@CMAKE_INSTALL_PREFIX@"
APP_NAME = "@APP_NAME@"
EXEC_NAME = "@EXEC_NAME@"
ICON_NAME = "@ICON_NAME@"
RESOURCE_NAME = "@RESOURCE_NAME@"

#temporary
FILE_PATH = "@PKG_DATADIR@/simply-todo"
#DATA_BASE = "@PKG_DATADIR@/simply.db"
DATA_BASE = "simply.db"

DEBUG = False

def use_local_dirs():
    print("Using Local Data Directories")

    global DEBUG
    DEBUG = True

    global PKG_DATADIR
    PKG_DATADIR = "data"

    global FILE_PATH
    FILE_PATH = join(PKG_DATADIR, "simply-todo")

    global DATA_BASE
    DATA_BASE = join(PKG_DATADIR, DATA_BASE)
