#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  widgets/headerBar.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from gi.repository import Gtk, GObject, Gio
import simply.config as config
from simply.widgets.notebookPopover import NotebookPopover

class SimplyHeaderBar(Gtk.Paned):

    #parent vars
    window = None

    #header bar items
    search_button = None
    notebook_button = None
    check_button = None
    tag_button = None
    import_button = None
    export_button = None
    menu_button = None

    #header bar objects
    left_headerbar = None
    right_headerbar = None
    combined_headerbar = None

    #app menu
    app_menu = None
    tag_item = None
    import_item = None
    export_item = None
    separator_item = None
    preferences_item = None
    quit_item = None

    def __init__(self, window):
        super(SimplyHeaderBar, self).__init__()

        self.window = window

        self.setup_layout()

        self.connect_signals()

        self.show_all()

    def setup_layout(self):
        #setup headerbars
        self.left_headerbar = Gtk.HeaderBar()
        self.left_headerbar.get_style_context().add_class("header-left")
        self.left_headerbar.set_show_close_button(True)
        self.right_headerbar = Gtk.HeaderBar()
        self.right_headerbar.get_style_context().add_class("header-right")
        self.right_headerbar.set_show_close_button(True)
        self.combined_headerbar = Gtk.HeaderBar()

        self.setup_window_buttons()

        #search button
        self.search_button = Gtk.Button()
        self.search_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="edit-find-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.search_button.add(img)
        self.search_button.set_tooltip_text("Search" + "…")
        self.search_button.set_sensitive(True)

        #notebook button
        self.notebook_button = Gtk.Button()
        self.notebook_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="pan-down-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.notebook_button.set_label("Notebook")
        self.notebook_button.set_image(img)
        self.notebook_button.props.always_show_image = True
        self.notebook_button.set_image_position(Gtk.PositionType.RIGHT)
        self.notebook_button.set_tooltip_text("Notebooks")
        self.notebook_button.set_sensitive(True)

        #check button
        self.check_button = Gtk.Button()
        self.check_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="object-select-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.check_button.add(img)
        self.check_button.set_tooltip_text("Check")
        self.check_button.set_sensitive(True)

        #tag button
        self.tag_button = Gtk.Button()
        self.tag_button.props.relief = Gtk.ReliefStyle.NONE #get rid of dat relief
        icon = Gio.ThemedIcon(name="bookmark-new-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.tag_button.add(img)
        self.tag_button.set_tooltip_text("Tag Note")
        self.tag_button.set_sensitive(True)

        #create app menu
        self.app_menu = Gtk.Menu()
        #menu items
        self.tag_item = Gtk.MenuItem("Tag Note" + "…")
        self.import_item = Gtk.MenuItem("Import")
        self.export_item = Gtk.MenuItem("Export")
        self.separator_item = Gtk.SeparatorMenuItem()
        self.preferences_item = Gtk.MenuItem("Preferences" + "…")
        self.quit_item = Gtk.MenuItem("Quit")
        #add items to menu
        self.app_menu.append(self.tag_item)
        self.app_menu.append(self.import_item)
        self.app_menu.append(self.export_item)
        self.app_menu.append(self.separator_item)
        self.app_menu.append(self.preferences_item)
        self.app_menu.append(self.quit_item)
        self.app_menu.show_all() #show all

        #set icon for menu button
        self.menu_button = Gtk.MenuButton()
        self.menu_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="application-menu-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.menu_button.add(img)
        self.menu_button.set_tooltip_text("Menu")
        self.menu_button.set_popup(self.app_menu)
        self.menu_button.set_sensitive(True)

        #import button
        self.import_button = Gtk.Button()
        self.import_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="document-import-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.import_button.add(img)
        self.import_button.set_tooltip_text("Import" + "…")
        self.import_button.set_sensitive(True)

        #import button
        self.export_button = Gtk.Button()
        self.export_button.props.relief = Gtk.ReliefStyle.NONE #get rid of relief
        icon = Gio.ThemedIcon(name="document-export-symbolic")
        img = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.export_button.add(img)
        self.export_button.set_tooltip_text("Export" + "…")
        self.export_button.set_sensitive(True)

        #add things to the left headerbar
        self.left_headerbar.pack_start(self.search_button)
        self.left_headerbar.set_custom_title(self.notebook_button)
        self.left_headerbar.pack_end(self.check_button)

        #add things to the right headerbar
        self.right_headerbar.pack_start(self.tag_button)
        self.right_headerbar.pack_end(self.menu_button)
        self.right_headerbar.pack_end(self.export_button)
        self.right_headerbar.pack_end(self.import_button)
        self.right_headerbar.set_title("Untitled Note")

        #add headerbar to panes
        self.pack1(self.left_headerbar, True, False)
        self.pack2(self.right_headerbar, True, False)
        self.get_style_context().add_class("header")
        self.set_position(257)

    def setup_window_buttons(self):
        buttons = Gtk.Settings.get_default().props.gtk_decoration_layout.split(":") #get array of gtk button layout

        if(len(buttons) < 2):
            print("Gtk Button Layout in unexpected format. Attempting to compensate")
            buttons = [buttons[0], ""]

        self.left_headerbar.set_decoration_layout(buttons[0] + ":")
        self.right_headerbar.set_decoration_layout(":" + buttons[1])

    def move_separator(self, position):
        self.set_position(position)

    def notebook_popup(self, event, param=None):
        popover = NotebookPopover()
        popover.set_relative_to(self.notebook_button)

        popover.show_all()

    def connect_signals(self):
        self.notebook_button.connect("clicked", self.notebook_popup) #watch for notebook press

        self.quit_item.connect("activate", self.window.destroy) #connect quit menu item to destroy window
