#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Window/PortraitWindow.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from gi.repository import Gtk, GObject, Gio

class NotebookPopover(Gtk.Popover):
    """docstring for NotebookPopover"""
    content = None #content box
    list_box = None #list box

    def __init__(self):
        super(NotebookPopover, self).__init__()

        self.content = Gtk.Box(orientation=Gtk.Orientation.VERTICAL) #create content
        self.add(self.content) #add content to this

        self.setup_layout()

        self.show_all()

    def setup_layout(self):
        self.list_box = Gtk.ListBox()

        for s in ["notebook 1", "notebook 2", "other notebook"]:
            row = NotebookRow(s)
            self.list_box.insert(row, len(self.list_box.get_children()))

        self.content.pack_start(self.list_box, False, False, 0)

        #separator
        sep = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        self.content.pack_start(sep, True, True, 0)

        #manage notebooks
        manage = Gtk.Button("Manage Notebooks" + "…")
        manage.get_style_context().add_class("list-row") #to make it fit in more
        self.content.pack_start(manage, False, False, 0)




class NotebookRow(Gtk.ListBoxRow):
    """docstring for NotebookRow"""

    content = None #content box
    notebook_title = ""

    def __init__(self, title):
        super(NotebookRow, self).__init__()

        self.notebook_title = title

        self.content = Gtk.Box(orientation=Gtk.Orientation.VERTICAL) #create content
        self.add(self.content)

        self.setup_layout()

        self.show_all()

    def setup_layout(self):
        self.get_style_context().add_class("popover-notebook-row") #so we can add some style

        self.content.pack_start(Gtk.Label(self.notebook_title), True, True, 0) #add label to box
