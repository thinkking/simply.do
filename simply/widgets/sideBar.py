#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  window/portraitWindow.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#!/usr/bin/python3.4

from gi.repository import Gtk, GObject, Gio, Pango
import os.path
import simply.config as config
from simply.core import Priority


class SideBar(Gtk.ListBox):
    """docstring for SideBar"""

    def __init__(self):
        super(SideBar, self).__init__() #init class
        self.setup_layout()

        self.props.width_request = 250

        #self.connect_signals()

        self.show_all()

    def setup_layout(self):
        test = [Priority.CRITICAL, Priority.HIGH, Priority.MEDIUM, Priority.LOW, Priority.NONE]
        if(os.path.exists(config.FILE_PATH)):
            tdfile = open(config.FILE_PATH, "r+")
            tdfile_list = tdfile.read().split('\n')
            i = 0
            for todo in tdfile_list:
                if todo != "":
                    row = SideBarRow(test[i], todo, "This is a fancy summary that is very long and because it is so long it will go beyond the limit of the sidebar", "Dec 7")
                    self.insert(row, len(self.get_children()))
                i += 1
                if (i >= len(test)): i = 0
            tdfile.close()
        else:
            print("Unable to read %s" % config.FILE_PATH)

class SideBarRow(Gtk.ListBoxRow):
    """docstring for SideBarRow"""

    #aspects
    color = None
    title = None
    summary = None
    date = None

    #widgets
    main = None #main box
    priority = None #color box
    content = None #content box
    summary_label = None #summary label

    def __init__(self, color, title, summary, date):
        super(SideBarRow, self).__init__() #init class

        self.color = color
        self.title = title
        self.summary = summary
        self.date = date

        self.setup_layout()

    def setup_layout(self):
        self.main = Gtk.Box()
        self.priority = Gtk.Box()
        self.content = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        #add some css tags for stylying
        self.priority.get_style_context().add_class("priority-box")
        self.content.get_style_context().add_class("content-box")

        #color
        self.priority.props.width_request = 2
        self.priority.get_style_context().add_class("priority-" + self.color)

        #title
        title_label = Gtk.Label(self.title)
        title_label.props.halign = Gtk.Align.START
        title_label.props.ellipsize = Pango.EllipsizeMode.END
        title_label.get_style_context().add_class("sidebar-title")

        #summary
        summary_label = Gtk.Label(self.summary)
        summary_label.props.halign = Gtk.Align.START
        summary_label.props.ellipsize = Pango.EllipsizeMode.END
        summary_label.get_style_context().add_class("sidebar-summary")

        #date
        date_label = Gtk.Label(self.date)
        date_label.props.halign = Gtk.Align.END
        date_label.get_style_context().add_class("sidebar-date")

        self.content.pack_start(title_label, True, True, 0)
        self.content.pack_start(summary_label, True, True, 0)
        self.content.pack_start(date_label, True, True, 0)

        self.main.pack_start(self.priority, False, False, 0)
        self.main.pack_start(self.content, True, True, 0)

        self.add(self.main)
