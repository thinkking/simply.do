#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
#
#  Window/PortraitWindow.py
#
#  Copyright 2016 Christopher Lee Murray <lee.christopher.murray@gmail.com>
#                             Sam Thomas <sgpthomas@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from gi.repository import Gtk, GObject, Gio
import simply.config as config
from simply.widgets.sideBar import SideBar
from simply.widgets.headerBar import SimplyHeaderBar
from simply.widgets.noteEditor import Editor
from simply.core.abstractDatabase import AbstractDatabase

# Class for portrait mode of application
class PortraitWindow(Gtk.Window):

    headerbar = None #headerbar object

    #menu items
    app_menu = None #menu object
    preferences_item = None
    quit_item = None

    #buttons
    search_button = None
    notebook_button = None
    import_button = None
    export_button = None
    menu_button = None #menu button object

    #paned view
    paned_view = None

    #sidebar
    scrolled_sidebar = None
    sidebar = None

    #editor
    scrolled_editor = None
    editor = None

    def __init__(self):
        #window init
        Gtk.Window.__init__(self, title=config.APP_NAME)

        database = AbstractDatabase()

        #window settings
        #self.set_border_width(7)
        self.set_default_size(900, 700) #save from dconf later

        self.create_headerbar()

        self.setup_layout()

        self.connect_signals()

        self.show_all()

    def create_headerbar(self):
        self.headerbar = SimplyHeaderBar(self)
        self.set_titlebar(self.headerbar)

    def setup_layout(self):
        self.paned_view = Gtk.Paned() #create pained view

        #set up sidebar widget
        self.scrolled_sidebar = Gtk.ScrolledWindow()
        self.sidebar = SideBar()
        self.scrolled_sidebar.props.hscrollbar_policy = Gtk.PolicyType.NEVER
        self.scrolled_sidebar.add(self.sidebar)

        #setup editor widget
        self.scrolled_editor = Gtk.ScrolledWindow()
        self.editor = Editor()
        self.scrolled_editor.add(self.editor)

        #paned settings
        #self.paned_view.props.min_position = self.sidebar.props.width_request

        #add everything to paned view and then to window
        self.paned_view.pack1(self.scrolled_sidebar, False, False)
        self.paned_view.pack2(self.scrolled_editor)
        self.add(self.paned_view)

    def destroy(self, event, param=None):
        #some stuffs
        Gtk.main_quit()

    def connect_signals(self):
        self.connect("delete-event", self.destroy) #connect close button to destroy window

        #popover for notebook switching
        #self.notebook_button.connect("clicked", self.notebook_popup)

        #self.quit_item.connect("activate", self.destroy) #connect quit menu item to destroy window
